
$('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('body').toggleClass('nav-open');
});

$(document).on('click touchstart', function(e) {

    if (!$(e.target).closest('.nav-main').length && !$(e.target).closest('.nav-toggle').length) {
        $('body').removeClass('nav-sidebar-open');
    }

    e.stopPropagation();
});


$(".btn-modal").fancybox({
    'padding'    : 0
});


var promoSlider = new Swiper ('.promoSlider', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 30,
    autoHeight: true,

    // If we need pagination
    pagination: {
        el: '.slider-pagination'
    }
});

$('.promo .slider-arrow.prev').on('click touchstart', function(e) {
    promoSlider.slidePrev();
});

$('.promo .slider-arrow.next').on('click touchstart', function(e) {
    promoSlider.slideNext();
});



$('.docs__title').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});



var gallerySlider = new Swiper ('.gallerySlider', {
    loop: true,
    slidesPerView: 2,
    spaceBetween: 30,
    autoHeight: true,

    // If we need pagination
    pagination: {
        el: '.slider-pagination'
    },
    breakpoints: {
        568: {
            slidesPerView: 1
        }
    }
});


$('.gallery .slider-arrow.prev').on('click touchstart', function(e) {
    gallerySlider.slidePrev();
});

$('.gallery .slider-arrow.next').on('click touchstart', function(e) {
    gallerySlider.slideNext();
});



